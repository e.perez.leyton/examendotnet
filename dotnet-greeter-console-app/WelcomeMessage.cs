﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnet_greeter_console_app
{
    public class WelcomeMessage
    {

        public static String Message(String name, DateTime date)
        {
            String msg = $"\nHello, {name}, Welcome to CP-DCB on {date:d} at {date:t}!";
            return msg;
        }

    }
}
