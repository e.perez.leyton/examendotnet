﻿using System;


namespace dotnet_greeter_console_app
{
    public class Greeter
    {
   
        static void Main(string[] args)
        {
            Console.WriteLine("\nWhat is your name? ");
            var name = Console.ReadLine();
            var date = DateTime.Now;
            // Console.WriteLine($"\nHello, {name}, Welcome to CP-DCB on {date:d} at {date:t}!");   
           
            Console.WriteLine(WelcomeMessage.Message(name, date));
            Console.Write("\nPress any key to exit...");
            Console.ReadKey(true);

        }
    }
}
