using NUnit.Framework;
using dotnet_greeter_console_app;
using System;

namespace Tests
{
    public class GreetingAppTests
    {
        [Test]
        public void check_welcome_message()
        {
            
            var name = "Narendra";
            var date = DateTime.Now;


            StringAssert.AreEqualIgnoringCase($"\nHello, {name}, Welcome to CP-DCB on {date:d} at {date:t}!", WelcomeMessage.Message(name, date), "Not matched");



        }
    }
}